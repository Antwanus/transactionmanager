package com.antoonvereecken.txmanager;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.math.BigDecimal;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Map<Integer, Account> accounts = new HashMap<>();

        //set up accounts
        for (int i  = 1; i <= 10; i++) {
            accounts.put(i, new Account(i, new BigDecimal(1000)));
        }

        //source to generate 1 transaction every x second
        Source<Integer, NotUsed> source = Source.repeat(1).throttle(1, Duration.ofSeconds(2));

        //flow to create a transfer (= 2 transactions = 2 accounts = debit & credit)
        Flow<Integer, Transfer, NotUsed> generateTransfer = Flow.of(Integer.class).map (x -> {
            Random r = new Random();
            int accountFrom = r.nextInt(9) + 1;
            int accountTo;
            do {
                 accountTo = r.nextInt(9) + 1;
            } while (accountTo == accountFrom);

            BigDecimal amount = new BigDecimal(r.nextInt(100000)).divide(new BigDecimal(100));
            Date date = new Date();

            Transaction from = new Transaction(accountFrom, BigDecimal.ZERO.subtract(amount), date);
            Transaction to = new Transaction(accountTo, amount, date);
            return new Transfer(from,to);
        });
        //flow to get the 2 transactions out of the transfer
        Flow<Transfer, Transaction, NotUsed> extractTransaction = Flow.of(Transfer.class)
                .mapConcat(
                        transfer -> List.of(transfer.getFrom(), transfer.getTo())
        );

        // create stream: i = 1; ++i;
        Source<Integer, NotUsed> txIdSource = Source.fromIterator(
                () -> Stream.iterate(1, integer -> integer + 1).iterator()
        );
        Sink<Transfer, CompletionStage<Done>> transferLoggerSink = Sink.foreach( transfer -> {
            System.out.println("Transfer from " + transfer.getFrom().getAccountNumber() + " to "
            + transfer.getTo().getAccountNumber() + " of " + transfer.getFrom().getAmount());
        });
        Flow<Transaction, Transaction, NotUsed> applyTxToAccountsFlow = Flow.of(Transaction.class)
                .map(tx -> {
                    Account account = accounts.get(tx.getAccountNumber());
                    account.addTransaction(tx);
                    System.out.println("ACCOUNT: " +  tx.getAccountNumber() + ", BALANCE: " + account.getBalance());
                    return tx;
                });
        Sink<Transaction, CompletionStage<Done>> rejectedTxSink = Sink.foreach( tx -> {
            System.out.println("-TX REJECTED " + tx + "\n--BALANCE " + accounts.get(tx.getAccountNumber()).getBalance());
        });

        RunnableGraph<CompletionStage<Done>> graph = RunnableGraph.fromGraph(GraphDSL.create(
                Sink.foreach(System.out::println),
                (builder, out) -> {
                    // source(id-gen) & flow(transaction>) to fanIn(zip id to tx)
                    FanInShape2<Transaction, Integer, Transaction> assignIdToTxFanS2 = builder.add(
                            ZipWith.create((trans, id) -> {
                                trans.setUniqueId(id);
                                return trans;
                            }));

                    builder.from(builder.add(source))
                                .via(builder.add(generateTransfer.alsoTo(transferLoggerSink)))
                                .via(builder.add(extractTransaction))
                            .toInlet(assignIdToTxFanS2.in0());
                    builder.from(builder.add(txIdSource))
                            .toInlet(assignIdToTxFanS2.in1());
                    builder.from(assignIdToTxFanS2.out())
                                .via(builder.add(Flow.of(Transaction.class)
                                    .divertTo(rejectedTxSink,
                                        tx -> {
                                            Account a = accounts.get(tx.getAccountNumber());
                                            BigDecimal newBalance = a.getBalance().add(tx.getAmount());
                                            return newBalance.compareTo(BigDecimal.ZERO) < 0 ? true : false;
                                })))
                                .via(builder.add(applyTxToAccountsFlow))
                            .to(out);

                    return ClosedShape.getInstance();
                }));

        /**CHAPTER 14 - PARTIAL (NON RUNNABLE) GRAPHS*/
        Graph<SourceShape<Transaction>, NotUsed> pGraph1of2 = GraphDSL.create(
                builder -> {
                    FanInShape2<Transaction, Integer, Transaction> assignIdToTxFanS2 = builder.add(
                            ZipWith.create((trans, id) -> {
                                trans.setUniqueId(id);
                                return trans;
                            }));
                    builder.from(builder.add(source))
                            .via(builder.add(generateTransfer.alsoTo(transferLoggerSink)))
                            .via(builder.add(extractTransaction))
                            .toInlet(assignIdToTxFanS2.in0());
                    builder.from(builder.add(txIdSource))
                            .toInlet(assignIdToTxFanS2.in1());
                    return SourceShape.of(assignIdToTxFanS2.out());
                }
        );
        Graph<SinkShape<Transaction>, CompletionStage<Done>> pGraph2of2 = GraphDSL.create(
                Sink.foreach(System.out::println),
                (builder, out) -> {
                    FlowShape<Transaction, Transaction> entrypointFlow = builder.add(Flow.of(Transaction.class)
                            .divertTo(rejectedTxSink, tx -> {
                                    Account a = accounts.get(tx.getAccountNumber());
                                    BigDecimal newBalance = a.getBalance().add(tx.getAmount());
                                    return newBalance.compareTo(BigDecimal.ZERO) < 0 ? true : false;
                            }));

                    builder.from(entrypointFlow)
                            .via(builder.add(applyTxToAccountsFlow))
                            .to(out);
                    return SinkShape.of(entrypointFlow.in());
                }
        );
//        RunnableGraph<CompletionStage<Done>> graph2 = RunnableGraph.fromGraph(GraphDSL.create(
//                pGraph2of2,
//                (builder, out) -> {
//                    builder.from(builder.add(pGraph1of2)).to(out);
//
//                    return ClosedShape.getInstance();
//                }));


        ActorSystem<?> actorSys = ActorSystem.create(Behaviors.empty(), "actorSystem");
//        graph2.run(actorSys);
        Source<Transaction, NotUsed> newSource = Source.fromGraph(pGraph1of2);
        newSource.to(pGraph2of2).run(actorSys);


    }
}
