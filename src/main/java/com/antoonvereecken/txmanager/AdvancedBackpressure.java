package com.antoonvereecken.txmanager;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class AdvancedBackpressure {

    public static void main(String[] args) {
        ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");

        Source<Integer, NotUsed> source = Source.fromIterator(() -> Stream.iterate(1, i -> i + 1).iterator())
                .throttle(5, Duration.ofSeconds(1));
//                .conflate( (a, b) -> { return a + b }); (we return same datatype

        Flow<Integer, Integer, NotUsed> conflateFlow = Flow.of(Integer.class)
                .conflate((accumulator, element) -> accumulator + element);

        Flow<Integer, List, NotUsed> conflateWithSeedFlow = Flow.of(Integer.class)
                .conflateWithSeed(  // if we change datatype we need seed
                        seed -> {
                            List<Integer> list = new ArrayList<>();
                            list.add(seed);
                            return list;
                        },
                        (list, newItem) -> {
                            list.add(newItem);
                            return list;
                        }
        );

        Flow<List, String, NotUsed> slowFlow = Flow.of(List.class).map(x -> {
            System.out.println("Flowing " + x);
            return x.toString();
        }).throttle(1, Duration.ofSeconds(1));

        Sink<String, CompletionStage<Done>> sink = Sink.foreach(x -> System.out.println("Sinking " + x));

        source.via(conflateWithSeedFlow).via(slowFlow).to(sink).run(actorSystem);
        
    }
}
