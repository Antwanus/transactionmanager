package com.antoonvereecken.txmanager;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.*;
import akka.stream.javadsl.*;
import akka.stream.typed.javadsl.ActorFlow;
import akka.stream.typed.javadsl.ActorSink;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class MainWithActors {
    public static void main(String[] args) {
        //source to generate 1 transaction every x second
        Source<Integer, NotUsed> source = Source.repeat(1).throttle(1, Duration.ofSeconds(2));

        //flow to create a transfer (= 2 transactions = 2 accounts = debit & credit)
        Flow<Integer, Transfer, NotUsed> generateTransfer = Flow.of(Integer.class).map (x -> {
            Random r = new Random();
            int accountFrom = r.nextInt(9) + 1;
            int accountTo;
            do {
                accountTo = r.nextInt(9) + 1;
            } while (accountTo == accountFrom);

            BigDecimal amount = new BigDecimal(r.nextInt(100000)).divide(new BigDecimal(100));
            Date date = new Date();

            Transaction from = new Transaction(accountFrom, BigDecimal.ZERO.subtract(amount), date);
            Transaction to = new Transaction(accountTo, amount, date);
            return new Transfer(from,to);
        });
        //flow to get the 2 transactions out of the transfer
        Flow<Transfer, Transaction, NotUsed> extractTransaction = Flow.of(Transfer.class)
                .mapConcat(
                        transfer -> List.of(transfer.getFrom(), transfer.getTo())
                );

        // create stream: i = 1; ++i;
        Source<Integer, NotUsed> txIdSource = Source.fromIterator(
                () -> Stream.iterate(1, integer -> integer + 1).iterator()
        );
        Sink<Transfer, CompletionStage<Done>> transferLoggerSink = Sink.foreach(transfer -> {
            System.out.println("Transfer from " + transfer.getFrom().getAccountNumber() + " to "
                    + transfer.getTo().getAccountNumber() + " of " + transfer.getFrom().getAmount());
        });


        /**CHAPTER 14 - PARTIAL (NON RUNNABLE) GRAPHS*/
        Graph<SourceShape<Transaction>, NotUsed> pGraph1of2 = GraphDSL.create(
                builder -> {
                    FanInShape2<Transaction, Integer, Transaction> assignIdToTxFanS2 = builder.add(
                            ZipWith.create((trans, id) -> {
                                trans.setUniqueId(id);
                                return trans;
                            }));
                    builder.from(builder.add(source))
                            .via(builder.add(generateTransfer.alsoTo(transferLoggerSink)))
                            .via(builder.add(extractTransaction))
                            .toInlet(assignIdToTxFanS2.in0());
                    builder.from(builder.add(txIdSource))
                            .toInlet(assignIdToTxFanS2.in1());
                    return SourceShape.of(assignIdToTxFanS2.out());
                }
        );
        ActorSystem<AccountManager.AccountManagerCommand> accountManager = ActorSystem.create(AccountManager.create(), "accountManager");

        Flow<Transaction, AccountManager.AddTransactionResponse, NotUsed> validateTxFlow =
                ActorFlow.ask(
                        accountManager,
                        Duration.ofSeconds(10),
//                        (tx, replyTo) -> new AccountManager.AddTransactionCommand(tx, replyTo)
                        AccountManager.AddTransactionCommand::new
                );

        Sink<AccountManager.AddTransactionResponse, CompletionStage<Done>> rejectedTxSink = Sink.foreach(tx -> {
            System.out.println("--- TX REJECTED --- " + tx.getTransaction());
        });

        Flow<AccountManager.AddTransactionResponse, AccountManager.AccountManagerCommand, NotUsed> validationResultFlow =
                Flow.of(AccountManager.AddTransactionResponse.class).map(result -> {
                    System.out.println("LOGGING TX:   " + result.getTransaction());
                    return new AccountManager.DisplayBalanceCommand(result.getTransaction().getAccountNumber());
                });

        Sink<AccountManager.AccountManagerCommand, NotUsed> displayBalanceSink =
                ActorSink.actorRef(
                        accountManager,
                        new AccountManager.CompleteCommand(),
                        (throwable) -> new AccountManager.FailedCommand()
                );

        Source<Transaction, NotUsed> newSource = Source.fromGraph(pGraph1of2);

        newSource
                    .via(validateTxFlow
                            .divertTo(rejectedTxSink,
                                    addTxResponse -> !addTxResponse.getSucceeded()
                    ))
                    .via(validationResultFlow)
                .to(displayBalanceSink)
            .run(accountManager);



    }
}
